package utils

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestChangeFileExtension(t *testing.T) {
	assert.Equal(t, "file.txt", ChangeFileExtension("file.doc", "txt"))
	assert.Equal(t, "file.txt", ChangeFileExtension("file.doc", ".txt"))
	assert.Equal(t, "file.txt", ChangeFileExtension("file.txt", "txt"))
}
