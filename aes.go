package utils

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"io"
)

// AES cryptor
type AES struct {
	gcm cipher.AEAD
}

// NewAES cryptor
func NewAES(key16or32 string) (*AES, error) {
	c, err := aes.NewCipher(ZAtob(key16or32))
	if err != nil {
		return nil, err
	}
	gcm, err := cipher.NewGCM(c)
	if err != nil {
		return nil, err
	}
	return &AES{gcm: gcm}, nil
}

// Encrypt data
func (c *AES) Encrypt(data []byte) []byte {
	nonce := make([]byte, c.gcm.NonceSize())
	if _, err := io.ReadFull(rand.Reader, nonce); err != nil {
		panic(err)
	}
	return c.gcm.Seal(nonce, nonce, data, nil)
}

// Decrypt data
func (c *AES) Decrypt(crypted []byte) ([]byte, error) {
	nonceSize := c.gcm.NonceSize()
	if len(crypted) < nonceSize {
		return nil, io.EOF
	}
	return c.gcm.Open(nil, crypted[:nonceSize], crypted[nonceSize:], nil)
}

// EncryptToBase64 data
func (c *AES) EncryptToBase64(text string) string {
	crypted := c.Encrypt(ZAtob(text))
	return base64.StdEncoding.EncodeToString(crypted)
}

// DecryptFromBase64 data
func (c *AES) DecryptFromBase64(crypted64 string) (string, error) {
	crypted, err := base64.StdEncoding.DecodeString(crypted64)
	if err != nil {
		return "", err
	}
	data, err := c.Decrypt(crypted)
	if err != nil {
		return "", err
	}
	return ZBtoa(data), nil
}
