package utils

import (
	"os"
	"os/signal"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"syscall"
)

// Workdir returns executable working dir
func Workdir() string {
	wd, _ := os.Getwd()
	return wd
}

// WorkdirDSN returns executable working dir as DSN path
//
// Example on windows: c/users/user/Desktop
func WorkdirDSN() string {
	if runtime.GOOS != "windows" {
		return Workdir()
	}
	if strings.Contains(os.Args[0], "\\go-build") {
		dsn := strings.Replace(Workdir(), ":", "", 1)
		dsn = strings.ReplaceAll(dsn, "\\", "/")
		return dsn
	}
	return ""
}

// CallerFile returns caller file path and line
func CallerFile(deep int) (path string, line int) {
	_, path, line, _ = runtime.Caller(deep)
	return
}

// CallerFilePath returns path to caller file
func CallerFilePath(deep int) string {
	_, path, _, _ := runtime.Caller(deep)
	return path
}

// CallerFileName returns name of caller file
func CallerFileName(deep int) string {
	_, path, _, _ := runtime.Caller(deep)
	_, file := filepath.Split(path)
	return file[:len(file)-3] // filename.go - ".go"
}

// WaitForExit signal
func WaitForExit() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
	<-c
}

// RuntimePkgIndex by caller
func RuntimePkgIndex(file, funcName string) int {
	i := len(file)
	for n := strings.Count(funcName, "/") + 2; n > 0; n-- {
		if i = strings.LastIndexByte(file[:i], '/'); i == -1 {
			break
		}
	}
	return i + 1
}

// RuntimePkgPath ...
func RuntimePkgPath() string {
	pc, file, _, _ := runtime.Caller(0)
	fn := runtime.FuncForPC(pc)
	if fn == nil {
		return "unknown"
	}
	return file[:RuntimePkgIndex(file, fn.Name())]
}

// RuntimePkgFileLineByPC caller
func RuntimePkgFileLineByPC(pc uintptr) (file string, line int) {
	fn := runtime.FuncForPC(pc)
	file, line = fn.FileLine(pc)
	pkgIndex := RuntimePkgIndex(file, fn.Name())
	file = file[pkgIndex:]
	return
}

// RuntimePkgFileLineStrByPC caller
func RuntimePkgFileLineStrByPC(pc uintptr) string {
	file, line := RuntimePkgFileLineByPC(pc)
	return file + ":" + strconv.Itoa(line)
}
