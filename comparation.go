package utils

import "crypto/subtle"

// ConstantTimeCompareStrings uses zero alloc converting to bytes
func ConstantTimeCompareStrings(a, b string) bool {
	return subtle.ConstantTimeCompare(ZAtob(a), ZAtob(b)) == 1
}
