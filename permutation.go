package utils

// Perm calls f with each permutation of s.
func Perm(s []string, f func([]string)) {
	perm(s, f, 0)
}

// Permute the values at index i to len(s)-1.
func perm(s []string, f func([]string), i int) {
	if i > len(s) {
		f(s)
		return
	}
	perm(s, f, i+1)
	for j := i + 1; j < len(s); j++ {
		s[i], s[j] = s[j], s[i]
		perm(s, f, i+1)
		s[i], s[j] = s[j], s[i]
	}
}
