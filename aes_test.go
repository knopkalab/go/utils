package utils

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAES(t *testing.T) {
	c, err := NewAES("keykeykeywefwfwuuguguogyuogugyug")
	assert.NoError(t, err)

	crypted64 := c.EncryptToBase64("text")

	rawText, err := c.DecryptFromBase64(crypted64)
	assert.NoError(t, err)

	assert.Equal(t, "text", rawText)
}
